<?php 
session_start();
$newCash = $_SESSION['currencies'];
$currencies = [
    'uah' => [
       'name' => 'Гривна',
       'course' => 1,
    ],
    'usd' => [
       'name' => 'Доллар',
       'course' => 27.1,
    ],
    'eur' => [
       'name' => 'Евро',
       'course' => 30.2,
    ],
 ];

$data = [
 [
    'title' => 'Кеды Adidas Originals Stan Smith',
    'price_val' => 1200,
    'discount_type' => 'value',
    'discount_val' => 200, 
  ],
  [
    'title' => 'Кроссовки Nike Tanjun',
    'price_val' => 1100,
    'discount_type' => 'value',
    'discount_val' => 300, 
  ],
  [
    'title' => 'Кроссовки Nike Venture',
    'price_val' => 700,
    'discount_type' => 'percent',
    'discount_val' => 20, 
  ],
  [
    'title' => 'Кроссовки Jordan 13',
    'price_val' => 2300,
    'discount_type' => 'value',
    'discount_val' => 300, 
  ],
  [
    'title' => 'Кроссовки Adidas zx',
    'price_val' => 1500,
    'discount_type' => 'percent',
    'discount_val' => 20, 
  ],
  [
    'title' => 'Кеды Adidas Samba',
    'price_val' => 1230,
    'discount_type' => 'value',
    'discount_val' => 100, 
  ],
  [
    'title' => 'Кроссовки Anta X-Game',
    'price_val' => 1250,
    'discount_type' => 'percent',
    'discount_val' => 30, 
  ],
  [
    'title' => 'Kроссоки Nike Zoom 10',
    'price_val' => 1750,
    'discount_type' => 'percent',
    'discount_val' => 15, 
  ],
  [
    'title' => 'Кроссовки Jordan Zero',
    'price_val' => 1700,
    'discount_type' => 'value',
    'discount_val' => 300, 
  ],
  [
    'title' => 'Кеды Puma Black',
    'price_val' => 1360,
    'discount_type' => 'percent',
    'discount_val' => 40, 
  ],
];

$currency = $currencies['uah'];
if(!empty($_SESSION['currencies'])){
    $currency = $currencies[$_SESSION['currencies']];
};
$course = $currency['course'];

function getPriceWithDiscount($price_val, $discount_type, $discount_val) {
  if($discount_type == 'value'){
      $price = $price_val - $discount_val;
      return round($price, 2);
  }elseif($discount_type == 'percent'){
      $price = $price_val*((100-$discount_val)/100);
      return round($price, 2);
  };
};
function convertPrice($price_val, $course){
  $priceEnd = round($price_val / $course, 2);
  return $priceEnd;
};
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Document</title>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
 </head>
 <body>
 <div class="container">
 <h1>MEGASPORT</h1>
 <form action="/setup.php" method="POST">
      <label>
         <select class="form-select" name="currencies">
         <option value="uah" selected>Укажите валюту</option> 
         <option value="uah"><?= $currencies['uah']['name']; ?></option>
         <option value="usd"><?= $currencies['usd']['name']; ?></option>
         <option value="eur"><?= $currencies['eur']['name']; ?></option>
         </select>
      </label>
      <button class="btn-warning">Refresh</button>
</form>
</div>
<div class="container">
<table class="table table-striped">
   <tr>
      <th>#</th>
      <th>Shoes</th>
      <th>price in ₴</th>
      <th>Discount product price in currency: <?=$currency['name'];?></th>
   </tr>
   <?php foreach($data as $key => $product): extract($product);  ?>   
    <tr>
        <th scope="row"><?= ++$key; ?></th> 
        <td><?= $title; ?></td>
        <td><?= $price_val * $currencies['uah']['course']; ?></td>
        <td><?=convertPrice(getPriceWithDiscount($price_val, $discount_type, $discount_val), $course); ?></td>
    </tr>
    <?php endforeach; ?>
</table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
 </body>
 </html>